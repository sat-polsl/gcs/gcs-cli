# GCS-CLI

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/sat-polsl/gcs/gcs-cli?branch=main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/sat-polsl/gcs/gcs-cli?)](https://goreportcard.com/report/gitlab.com/sat-polsl/gcs/gcs-cli)
[![codecov](https://codecov.io/gl/sat-polsl:gcs/gcs-cli/branch/main/graph/badge.svg?token=ZnpFOit210)](https://codecov.io/gl/sat-polsl:gcs/gcs-cli)
[![Go Reference](https://pkg.go.dev/badge/gitlab.com/sat-polsl/gcs/gcs-cli.svg)](https://pkg.go.dev/gitlab.com/sat-polsl/gcs/gcs-cli)
