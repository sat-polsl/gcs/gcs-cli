/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package printer

import (
	"encoding/json"
	"os"

	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
	"gopkg.in/yaml.v3"
)

type Missions struct {
	Mission map[int]Body `yaml:"items"`
}

type Body struct {
	Name        string            `yaml:"name"`
	Description *string           `yaml:"description"`
	Fields      map[string]string `yaml:"fields"`
}

type Field struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

func (m *Missions) FromAPI(missions []apiv1.Mission) error {
	if m.Mission == nil {
		m.Mission = make(map[int]Body)
	}

	for _, mission := range missions {
		body := Body{
			Name:        mission.Name,
			Description: mission.Description,
		}

		fields := []Field{}
		err := json.Unmarshal([]byte(mission.Fields), &fields)
		if err != nil {
			return err
		}

		if body.Fields == nil {
			body.Fields = make(map[string]string)
		}

		for _, field := range fields {
			body.Fields[field.Name] = field.Type
		}

		m.Mission[mission.Id] = body
	}

	return nil
}

func Print(missions []apiv1.Mission) error {
	m := &Missions{}

	err := m.FromAPI(missions)
	if err != nil {
		return err
	}

	enc := yaml.NewEncoder(os.Stdout)
	enc.SetIndent(2)

	return enc.Encode(m)
}
