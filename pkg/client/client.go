/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package client

import (
	"context"
	"encoding/json"
	"fmt"
	"io"

	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
)

type Client struct {
	client *apiv1.Client
}

func New(addr string) (*Client, error) {
	client, err := apiv1.NewClient(addr)
	if err != nil {
		return nil, err
	}

	return &Client{
		client: client,
	}, nil
}

func (c *Client) ListMissions(ctx context.Context) ([]apiv1.Mission, error) {
	res, err := c.client.GetMissions(ctx, &apiv1.GetMissionsParams{})
	if err != nil {
		return nil, fmt.Errorf("failed to get missions: %w", err)
	}

	switch res.StatusCode {
	case 200:
		var missions []apiv1.Mission
		if err := json.NewDecoder(res.Body).Decode(&missions); err != nil {
			return nil, fmt.Errorf("failed to decode missions: %w", err)
		}

		return missions, nil

	case 500:
		errRes := &InternalError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return nil, fmt.Errorf("failed to decode error response: %w", err)
		}

		return nil, errRes

	default:
		return nil, fmt.Errorf("unexpected status code: %d", res.StatusCode)
	}
}

func (c *Client) GetMission(ctx context.Context, ids ...string) ([]apiv1.Mission, error) {
	var missions []apiv1.Mission

	for _, id := range ids {
		res, err := c.client.GetMission(ctx, id)
		if err != nil {
			return nil, fmt.Errorf("failed to get mission: %w", err)
		}

		switch res.StatusCode {
		case 200:
			var mission *apiv1.Mission
			if err := json.NewDecoder(res.Body).Decode(&mission); err != nil {
				return nil, fmt.Errorf("failed to decode mission: %w", err)
			}

			missions = append(missions, *mission)

		case 404:
			errRes := &NotFoundError{}
			if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
				return nil, fmt.Errorf("failed to decode error response: %w", err)
			}

			return nil, errRes

		case 500:
			errRes := &InternalError{}
			if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
				return nil, fmt.Errorf("failed to decode error response: %w", err)
			}

			return nil, errRes

		default:
			return nil, fmt.Errorf("unexpected status code: %d", res.StatusCode)
		}
	}

	return missions, nil
}

func (c *Client) CreateMission(ctx context.Context, mission apiv1.Mission) (apiv1.Mission, error) {
	res, err := c.client.CreateMission(ctx, mission)
	if err != nil {
		return apiv1.Mission{}, fmt.Errorf("failed to create mission: %w", err)
	}

	switch res.StatusCode {
	case 201:
		var mission apiv1.Mission
		if err := json.NewDecoder(res.Body).Decode(&mission); err != nil {
			return apiv1.Mission{}, fmt.Errorf("failed to decode mission: %w", err)
		}

		return mission, nil

	case 400:
		errRes := &ValidationError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return apiv1.Mission{}, fmt.Errorf("failed to decode error response: %w", err)
		}

		return apiv1.Mission{}, errRes

	case 500:
		errRes := &InternalError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return apiv1.Mission{}, fmt.Errorf("failed to decode error response: %w", err)
		}

		return apiv1.Mission{}, errRes

	default:
		return apiv1.Mission{}, fmt.Errorf("unexpected status code: %d", res.StatusCode)
	}
}

func (c *Client) UpdateMission(ctx context.Context, id string, mission apiv1.Mission) (apiv1.Mission, error) {
	res, err := c.client.UpdateMission(ctx, id, mission)
	if err != nil {
		return apiv1.Mission{}, fmt.Errorf("failed to update mission: %w", err)
	}

	switch res.StatusCode {
	case 200:
		var mission apiv1.Mission
		if err := json.NewDecoder(res.Body).Decode(&mission); err != nil {
			return apiv1.Mission{}, fmt.Errorf("failed to decode mission: %w", err)
		}

		return mission, nil

	case 400:
		errRes := &ValidationError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return apiv1.Mission{}, fmt.Errorf("failed to decode error response: %w", err)
		}

		return apiv1.Mission{}, errRes

	case 404:
		errRes := &NotFoundError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return apiv1.Mission{}, fmt.Errorf("failed to decode error response: %w", err)
		}

		return apiv1.Mission{}, errRes

	case 500:
		errRes := &InternalError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return apiv1.Mission{}, fmt.Errorf("failed to decode error response: %w", err)
		}

		return apiv1.Mission{}, errRes

	default:
		return apiv1.Mission{}, fmt.Errorf("unexpected status code: %d", res.StatusCode)
	}
}

func (c *Client) DeleteMission(ctx context.Context, id string) error {
	res, err := c.client.DeleteMission(ctx, id)
	if err != nil {
		return fmt.Errorf("failed to delete mission: %w", err)
	}

	switch res.StatusCode {
	case 204:
		return nil

	case 404:
		errRes := &NotFoundError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return fmt.Errorf("failed to decode error response: %w", err)
		}

		return errRes

	case 500:
		errRes := &InternalError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return fmt.Errorf("failed to decode error response: %w", err)
		}

		return errRes

	default:
		return fmt.Errorf("unexpected status code: %d", res.StatusCode)
	}
}

func (c *Client) GetGrafanaTemplate(ctx context.Context, id string) ([]byte, error) {
	res, err := c.client.GetGrafanaTemplate(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("failed to get grafana template: %w", err)
	}

	switch res.StatusCode {
	case 200:
		b, err := io.ReadAll(res.Body)
		if err != nil {
			return nil, fmt.Errorf("failed to read grafana template: %w", err)
		}

		return b, nil

	case 404:
		errRes := &NotFoundError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return nil, fmt.Errorf("failed to decode error response: %w", err)
		}

		return nil, errRes

	case 500:
		errRes := &InternalError{}
		if err := json.NewDecoder(res.Body).Decode(errRes); err != nil {
			return nil, fmt.Errorf("failed to decode error response: %w", err)
		}

		return nil, errRes

	default:
		return nil, fmt.Errorf("unexpected status code: %d", res.StatusCode)
	}
}
