/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package compiler

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/hashicorp/go-multierror"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
	"gopkg.in/yaml.v3"
)

type Mission struct {
	Name        string            `yaml:"name" json:"name"`
	Description string            `yaml:"description" json:"description"`
	Fields      map[string]string `yaml:"fields" json:"fields"`
}

type Field struct {
	Name string `yaml:"name" json:"name"`
	Type string `yaml:"type" json:"type"`
}

func (m *Mission) ToAPI() (apiv1.Mission, error) {
	// marshal fields to plain string in JSON format

	var fields []Field
	for k, v := range m.Fields {
		fields = append(fields, Field{
			Name: k,
			Type: v,
		})
	}

	b, err := json.Marshal(fields)
	if err != nil {
		return apiv1.Mission{}, err
	}

	return apiv1.Mission{
		Name:        m.Name,
		Description: &m.Description,
		Fields:      string(b),
	}, nil
}

func (m *Mission) UnmarshalYAML(b []byte) error {
	var raw map[string]interface{}
	err := yaml.Unmarshal(b, &raw)
	if err != nil {
		return err
	}

	if fields, ok := raw["fields"].(map[string]string); !ok {
		return fmt.Errorf("mission fields must be a map of strings")
	} else {
		var result error
		for k, v := range fields {
			switch v {
			case "TEXT", "INTEGER", "DOUBLE PRECISION", "BOOLEAN":
				// do nothing
			default:
				result = multierror.Append(result, fmt.Errorf("field '%s' must be one of: TEXT, INTEGER, DOUBLE PRECISION, BOOLEAN, got: '%s'", k, v))
			}
		}
	}

	type Plain Mission
	var plain Plain
	if err := yaml.Unmarshal(b, &plain); err != nil {
		return err
	}
	*m = Mission(plain)

	return nil
}

func Compile(filename string) ([]byte, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to open mission file: %w", err)
	}
	defer f.Close()

	var m Mission
	if err := yaml.NewDecoder(f).Decode(&m); err != nil {
		return nil, fmt.Errorf("failed to decode mission file: %w", err)
	}

	apiMission, err := m.ToAPI()
	if err != nil {
		return nil, fmt.Errorf("failed to convert mission to API: %w", err)
	}

	return json.Marshal(apiMission)
}
