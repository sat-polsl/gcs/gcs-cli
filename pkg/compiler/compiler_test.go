/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package compiler

import (
	"reflect"
	"testing"

	"gopkg.in/yaml.v3"
)

var validMission = `name: Test mission
description: Test mission description
fields:
  field1: INREGER
  field2: TEXT
  field3: DOUBLE PRECISION
`

func TestMission_UnmarshalYAML(t *testing.T) {
	tests := []struct {
		name    string
		input   string
		want    *Mission
		wantErr error
	}{
		{
			name:  "valid mission",
			input: validMission,
			want: &Mission{
				Name:        "Test mission",
				Description: "Test mission description",
				Fields: map[string]string{
					"field1": "INREGER",
					"field2": "TEXT",
					"field3": "DOUBLE PRECISION",
				},
			},
			wantErr: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Mission{}
			err := yaml.Unmarshal([]byte(tt.input), m)
			if err != nil {
				t.Errorf("Mission.UnmarshalYAML() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(m, tt.want) {
				t.Errorf("Mission.UnmarshalYAML() got = %v, want %v", m, tt.want)
			}
		})
	}
}
