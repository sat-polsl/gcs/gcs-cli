VERSION ?= $(shell git tag | tail -n 1 | grep "" || echo 'v0.0.0')$(shell git diff --quiet || echo '-dev')

REGISTRY := registry.gitlab.com
NAME ?= $(shell sed -En 's/^module (.*)$$/\1/p' go.mod | cut -d / -f 4 )
REPOSITORY := $(shell sed -En 's/^module (.*)$$/\1/p' go.mod | cut -d / -f 2,3 )

TOOLCHAIN_VERSION := $(shell sed -En 's/^go (.*)$$/\1/p' go.mod)
MODULE_NAME := $(shell sed -En 's/^module (.*)$$/\1/p' go.mod)

GOOS ?= $(shell go env GOOS)
GOARCH ?= $(shell go env GOARCH)

GO_SETTINGS += GOOS=${GOOS} GOARCH=${GOARCH} CGO_ENABLED=0

GO ?= go

LDFLAGS += -X ${MODULE_NAME}/version.Version=${VERSION}
LDFLAGS += -X ${MODULE_NAME}/version.Name=${NAME}

.PHONY: build
build:
	${GO_SETTINGS} ${GO} build \
		-ldflags="${LDFLAGS}" \
		-o ./build/${NAME}${EXTRA}

.PHONY: build-release
build-release:
	LDFLAGS="-s -w" \
	EXTRA="-${GOOS}-${GOARCH}" \
		make build

.PHONY: lint
lint:
	golangci-lint run -v

.PHONY: clean
clean:
	rm -rf ./build

.PHONY: docs
docs:
	rm -rf ./docs/*.md
	${GO} run \
		-ldflags="${LDFLAGS}" \
		./scripts/docs-generator.go

.PHONY: e2e
e2e:
	TEST_MODE="-tags=e2e" make test

.PHONY: test
test:
	${GO} test ${TEST_MODE} \
		-cover \
		-race \
		-covermode=atomic \
		-coverprofile=coverage.out \
		./...
