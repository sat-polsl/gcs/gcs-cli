**First:** if you're unsure or afraid of anything, just ask or submit the issue or pull request anyways. You won't be yelled at for giving your best effort. The worst that can happen is that you'll be politely asked to change something. We appreciate all contributions!

For those folks who want a bit more guidance on the best way to contribute to the project, read on. Addressing the points below lets us merge or address your contributions quickly.

# Contributing to GCS repos

- [Issues](#issues)
    - [Reporitng an Issue](#reporting-an-issue)
- [Working with repsitory](#working-with-repository)
    - [Contribution flow](#contribution-flow)
    - [Git branching workflow](#git-branching-flow)
    - [Commit message format](#commit-message-format)
- [Go environment](#go-environment)

## Issues

### Reporting an Issue

* Make sure you test against the latest released version. It is possible
  we already fixed the bug you're experiencing.

* If you experienced a panic, please create a [gist](https://gist.github.com)
  of the *entire* generated crash log for us to look at. Double check
  no sensitive items were in the log.

* Respond as promptly as possible to any questions made by the _doctl_
  team to your issue. Stale issues will be closed.

## Working with repository

### Contribution flow

1. Create a topic branch from where you want to base your work (always `main`).
1. Make commits of logical units.
1. Make sure your commit messages are in the proper format (see [Commit message format](#commit-message-format))
1. Push your changes to a topic branch.
1. Submit a pull request to the `main` branch.
1. Make sure the tests pass, and add any new tests as appropriate.

### Git branching workflow

We follow a semi-strict convention for branch workflow, commonly known as [GitHub Flow](https://guides.github.com/introduction/flow/). Feature branches are used to develop new features for the upcoming releases. Must branch off from `main` and must merge into `main`.

Branch name shall be descriptive.

If commits will be referring to the issue, the issue number / tag of the issue shall be included in in the branch name.

```text
fix-123-request-race-prevention
```

### Commit message format

We follow a strict convention for commit messages, known as [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) that is designed to answer two questions: what changed and why. The subject line should feature the what and the body of the commit should describe the why and how.

```text
fix: prevent racing of requests

Introduce a request id and a reference to latest request. Dismiss
incoming responses other than from latest request.

Remove timeouts which were used to mitigate the racing issue but are
obsolete now.

Fixes: #123
```

The format can be described more formally as follows:

```text
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

The first line is the subject and should be no longer than 70 characters, the second line is always blank, and other lines should be wrapped at 80 characters. This allows the message to be easier to read on GitHub as well as in various git tools.

## Go environment

The minimal version of Golang is 1.18.

Be sure to run `go fmt` on your code before submitting a pull request.
