/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package main

import (
	"bytes"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"
	"gitlab.com/sat-polsl/gcs/gcs-cli/version"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/tools"
)

const (
	docsTemplateDir = "docs/template"
	docsDataDir     = "docs/template/data"
	docsDir         = "docs"
)

var (
	data = map[string]string{
		"Version": version.Version,
		"Date":    time.Now().Format(time.RFC822),
	}
)

func main() {
	myData, err := readData(docsDataDir)
	if err != nil {
		panic(err)
	}

	if err := execTemplates(docsTemplateDir, docsDir, myData); err != nil {
		panic(err)
	}
}

func fileNameWithoutExt(fileName string) string {
	return fileName[:len(fileName)-len(filepath.Ext(fileName))]
}

func readData(in string) (map[string]string, error) {
	out := data

	dirEntries, err := os.ReadDir(in)
	if err != nil {
		return nil, err
	}

	for _, f := range dirEntries {
		if f.IsDir() {
			continue
		}

		b, err := os.ReadFile(path.Join(in, f.Name()))
		if err != nil {
			return nil, err
		}

		name := ""
		if strings.HasSuffix(f.Name(), ".tpl") {
			t, err := template.New(f.Name()).Parse(string(b))
			if err != nil {
				return nil, err
			}

			buf := new(bytes.Buffer)
			if err := t.Execute(buf, data); err != nil {
				return nil, err
			}

			name = strcase.ToCamel(fileNameWithoutExt(fileNameWithoutExt(f.Name())))
			out[name] = buf.String()
		} else {
			name = strcase.ToCamel(fileNameWithoutExt(f.Name()))
			out[name] = string(b)
		}
		fmt.Printf("< %s\n", name)
	}

	return out, nil
}

func execTemplates(in, out string, data map[string]string) error {
	dirEntries, err := os.ReadDir(in)
	if err != nil {
		return err
	}

	for _, f := range dirEntries {
		if f.IsDir() {
			continue
		}

		if !strings.HasSuffix(f.Name(), ".tpl") {
			continue
		}

		t, err := template.ParseFiles(path.Join(in, f.Name()))
		if err != nil {
			return err
		}

		if err := executeTemplate(t, path.Join(out, fileNameWithoutExt(f.Name())), data); err != nil {
			return err
		}
	}

	return nil
}

func executeTemplate(t *template.Template, out string, data interface{}) error {
	fmt.Printf("> %s\n", out)

	f, err := os.OpenFile(out, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer tools.Wrap(f.Close)

	err = t.Execute(f, data)
	if err != nil {
		return err
	}

	return nil
}
