/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package missions

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/sat-polsl/gcs/gcs-cli/pkg/compiler"
)

func NewMissionCompileCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "compile",
		Short: "Compile a mission file",
		Long: `Compile a mission file from generic mission format to a format that can be used by the GCS
		
Example input (in YAML format):

name: 'mission' # name of the mission
description: |
  This is a mission description that can be as long as you want it to be.
fields:
  # name: 'type' where type is one of: TEXT, INTEGER, DOUBLE PRECISION, BOOLEAN
  altitude: 'DOUBLE PRECISION' # type must be format compatible with PostgreSQL
  latitude: 'DOUBLE PRECISION'
  longitude: 'DOUBLE PRECISION'
`,
		Args:          cobra.ExactArgs(1),
		SilenceErrors: true,
		SilenceUsage:  true,
		RunE: func(cmd *cobra.Command, args []string) error {
			b, err := compiler.Compile(args[0])
			if err != nil {
				return fmt.Errorf("failed to compile mission: %w", err)
			}

			if missionFile != "" {
				err = writeToFile(missionFile, b)
				if err != nil {
					return fmt.Errorf("failed to write to file: %w", err)
				}
			} else {
				fmt.Print(string(b))
			}

			return nil
		},
	}

	cmd.Flags().StringVarP(&missionFile, "output", "o", "", "File to save the compile to")

	return cmd
}

func writeToFile(path string, data []byte) error {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("failed to open file: %w", err)
	}
	defer f.Close()

	_, err = f.Write(data)
	if err != nil {
		return fmt.Errorf("failed to write to file: %w", err)
	}

	return nil
}
