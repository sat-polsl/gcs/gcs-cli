/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package missions

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
)

var (
	missionFile      string
	missionFromStdIn bool
)

func NewMissionsCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "missions",
		Aliases: []string{"mission"},
		Short:   "Manage missions",
		Long:    `Manage missions tracked by the GCS system`,
	}

	cmd.AddCommand(
		NewMissionCreateCmd(),
		NewMissionGetCmd(),
		NewMissionUpdateCmd(),
		NewMissionDeleteCmd(),

		NewMissionCompileCmd(),
		NewMissionGetGrafanaTemplateCmd(),
	)

	return cmd
}

func missionFromFile(file string) (apiv1.Mission, error) {
	var mission apiv1.Mission

	if file == "" {
		return apiv1.Mission{}, fmt.Errorf("mission file not specified")
	}

	f, err := os.Open(file)
	if err != nil {
		return apiv1.Mission{}, fmt.Errorf("failed to open mission file: %w", err)
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&mission)
	if err != nil {
		return apiv1.Mission{}, fmt.Errorf("failed to decode mission from file: %w", err)
	}

	return mission, nil
}
