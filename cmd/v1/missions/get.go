/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package missions

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sat-polsl/gcs/gcs-cli/pkg/client"
	"gitlab.com/sat-polsl/gcs/gcs-cli/pkg/printer"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
)

func NewMissionGetCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "get",
		Short:         "Get a mission",
		Long:          `Get a mission`,
		SilenceErrors: true,
		SilenceUsage:  true,
		RunE: func(cmd *cobra.Command, args []string) error {
			c, err := client.New(viper.GetString("server.address"))
			if err != nil {
				return fmt.Errorf("failed to create client: %w", err)
			}

			var missions []apiv1.Mission
			if len(args) == 0 {
				missions, err = c.ListMissions(cmd.Context())
				if err != nil {
					return fmt.Errorf("failed to list missions: %w", err)
				}
			} else {
				missions, err = c.GetMission(cmd.Context(), args...)
				if err != nil {
					return fmt.Errorf("failed to get mission: %w", err)
				}
			}

			return printer.Print(missions)
		},
	}

	return cmd
}
