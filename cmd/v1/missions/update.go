/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package missions

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sat-polsl/gcs/gcs-cli/pkg/client"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
)

func NewMissionUpdateCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "update",
		Short:         "Update a mission",
		Long:          `Update a mission`,
		Args:          cobra.ExactArgs(1),
		SilenceErrors: true,
		SilenceUsage:  true,
		RunE: func(cmd *cobra.Command, args []string) error {
			c, err := client.New(viper.GetString("server.address"))
			if err != nil {
				return fmt.Errorf("failed to create client: %w", err)
			}

			var mission apiv1.Mission

			if missionFromStdIn {
				err := json.NewDecoder(os.Stdin).Decode(&mission)
				if err != nil {
					return fmt.Errorf("failed to decode mission from stdin: %w", err)
				}
			} else {
				mission, err = missionFromFile(missionFile)
				if err != nil {
					return fmt.Errorf("failed to create mission from file: %w", err)
				}
			}

			_, err = c.UpdateMission(cmd.Context(), args[0], apiv1.Mission{})
			if err != nil {
				return fmt.Errorf("failed to update mission: %w", err)
			}

			return nil
		},
	}

	cmd.Flags().StringVarP(&missionFile, "file", "f", "", "mission description from file")
	cmd.Flags().BoolVarP(&missionFromStdIn, "stdin", "i", false, "read mission description from stdin")

	return cmd
}
