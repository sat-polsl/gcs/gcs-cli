/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package cmd

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/sat-polsl/gcs/gcs-cli/cmd/v1/missions"
	"gitlab.com/sat-polsl/gcs/gcs-cli/cmd/version"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "satgcs",
	Short: "satgcs is a command line interface for the GCS project",
	Long:  `satgcs is a command line interface for the GCS project`,
	RunE: func(cmd *cobra.Command, args []string) error {
		log.Info("what do you want me to do?")
		return cmd.Help()
	},
}

func init() {
	initFlags()
	initSubCmd()

	cobra.OnInitialize(initConfig, initLogger)
}

func initFlags() {
	rootCmd.PersistentFlags().String("log-level", "info", "log level")
	viper.BindPFlag("log_level", rootCmd.PersistentFlags().Lookup("log-level"))
}

func initSubCmd() {
	rootCmd.AddCommand(missions.NewMissionsCmd())
	rootCmd.AddCommand(version.NewVersionCmd())
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetDefault("log_level", "info")

	// Find home directory.
	home, err := os.UserHomeDir()
	if err != nil {
		log.WithField("err", err.Error()).Fatal("could not find home directory")
	}

	// Search config in context directory
	viper.AddConfigPath(".")
	// Search config in home directory
	viper.AddConfigPath(home)
	viper.SetConfigType("toml")
	viper.SetConfigName(".satgcs")
	viper.SetEnvKeyReplacer(strings.NewReplacer(`.`, `_`))

	viper.SetEnvPrefix("SAT")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	viper.ReadInConfig()
}

func initLogger() {
	level, err := log.ParseLevel(viper.GetString("log_level"))
	if err != nil {
		level = log.DebugLevel
	}

	log.SetLevel(level)
	log.WithField("level", level).Info("log level set")

	if file := viper.ConfigFileUsed(); file != "" {
		log.WithField("file", file).Debug("config file loaded")
	}
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		log.WithField("err", err.Error()).Fatal("failed to execute command")
	}
}
