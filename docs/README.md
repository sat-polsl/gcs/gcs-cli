# Table of Contents

- [Configuration Instructions](docs/CONFIGURATION.md)
- [Usage Instructions](docs/USAGE.md)

---

| _Version_ | `v0.3.1` |
|-----------|------------------|
| _Date_    | `15 May 23 21:35 CEST`    |
