# Configuration of the GCS Listener

## Configuration file

The configuration file is in TOML format. Application expects it to be placed either in `$HOME/.satgcs.toml` or in the current context.

```toml
{{ .ConfigExample }}
```

---

| _Version_ | `{{ .Version }}` |
|-----------|------------------|
| _Date_    | `{{ .Date }}`    |
