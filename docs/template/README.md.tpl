# Table of Contents

- [Configuration Instructions](docs/CONFIGURATION.md)
- [Usage Instructions](docs/USAGE.md)

---

| _Version_ | `{{ .Version }}` |
|-----------|------------------|
| _Date_    | `{{ .Date }}`    |
