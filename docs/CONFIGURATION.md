# Configuration of the GCS Listener

## Configuration file

The configuration file is in TOML format. Application expects it to be placed either in `$HOME/.satgcs.toml` or in the current context.

```toml
# `log_level` defines how verbose should the logging system be.
#
# Environment variable: `SAT_LOG_LEVEL`
#
# Possible values:
#   - "panic"
#   - "fatal"
#   - "error"
#   - "warning" - alias "warn"
#   - "info" - default
#   - "debug"
#   - "trace"
log_level = "info"

# `[server]` defines the configuration for the server.
[server]

# `address` defines the address on which the server will listen.
#
# Environment variable: `SAT_SERVER_ADDRESS`
address = "http://localhost:8080"

```

---

| _Version_ | `v0.3.1` |
|-----------|------------------|
| _Date_    | `15 May 23 21:35 CEST`    |
