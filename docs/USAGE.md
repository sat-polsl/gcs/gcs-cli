# Usage Instructions

## Prerequisites

Before you can use the SatGCS CLI, you need to create a configuration file. The configuration file is used to store the connection details to the SatGCS server. The details can be found [here](CONFIGURATION.md).

## Subcommands

### `missions`

Missions command is used to manage missions.

_Synatx:_

```bash
satgcs missions [command]
```

#### `create`

Create command is used to create a new mission. The command will read the mission from the standard input or file.

_Synatx:_

```bash
satgcs missions create [flags]
```

#### `compile`

Compile command is used to compile a mission from simple, human readable format to machine readable format. The command will read the mission from file and write the compiled mission to the standard output. This can be redirected to a file or can be piped to the `create` command.

_Synatx:_

```bash
satgcs missions compile [flags]
```

The syntax of the mission file is as follows:

```yaml
name: 'mission' # name of the mission
description: |
  This is a mission description that can be as long as you want it to be.
fields:
  # name: 'type' where type is one of: TEXT, INTEGER, DOUBLE PRECISION, BOOLEAN
  altitude: 'DOUBLE PRECISION' # type must be format compatible with PostgreSQL
  latitude: 'DOUBLE PRECISION'
  longitude: 'DOUBLE PRECISION'
```

The mission compiled from the above example is as follows:

```json
{"description":"This is a mission description that can be as long as you want it to be.\n","fields":"[{\"name\":\"altitude\",\"type\":\"DOUBLE PRECISION\"},{\"name\":\"latitude\",\"type\":\"DOUBLE PRECISION\"},{\"name\":\"longitude\",\"type\":\"DOUBLE PRECISION\"}]","id":0,"name":"mission"}
```

#### `get`

Get command is used to get a mission.

_Synatx:_

```bash
satgcs missions get [flags] <mission>
```

#### `update`

Update command is used to update a mission.

_Synatx:_

```bash
satgcs missions update [flags] mission
```

#### `delete`

> 🚧 This command is not yet implemented on the server side.

Delete command is used to delete a mission.

_Synatx:_

```bash
satgcs missions delete [flags] mission [mission...]
```

#### `grafana`

Grafana command is used to generate Grafana dashboards for a mission.

_Synatx:_

```bash
satgcs missions grafana [flags] mission
```

---

| _Version_ | `v0.3.1` |
|-----------|------------------|
| _Date_    | `15 May 23 21:35 CEST`    |
